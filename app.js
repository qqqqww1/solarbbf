import Koa from 'koa';

const app = new Koa();
import Router from 'koa-router';
import bodyParser from 'koa-bodyparser';

app.use(bodyParser());

import fetch from 'node-fetch';
// 缓存对象,key为请求路径,value为对应返回值
const cache = {};
const setResponse = {}

// 后端API地址
const backendUrl = 'http://localhost:666';

import axios from 'axios';
const axiosInstance = axios.create({
    timeout: 60000,
    baseURL: backendUrl,
    withCredentials: true,
});

const skipList = ['/cachedPaths', '/cacheValue', '/setCache', '/cancelSet']
app.use(async (ctx, next) => {
    const { path, headers, method, query } = ctx.request;
    if (skipList.includes(path)) {
        await next()
        return
    }
    const params = new URLSearchParams();
    Object.keys(query).forEach(key => {
        params.append(key, query[key]);
    });
    const fullUrl = path + '?' + params.toString();

    console.log(fullUrl)
    // 检查缓存
    if (setResponse[fullUrl]) {
        ctx.body = setResponse[fullUrl];
    } else {
        // 请求后端API
        let resp
        if (method == 'POST') {
            resp = await fetch(backendUrl + fullUrl, {
                method: 'POST',
                headers: {
                    ...headers,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(ctx.request.body),
                credentials: 'include',
            })
            cache[fullUrl] = resp;
            const body = await resp.json();
            const responseHeaders = resp.headers;


            // 在 Koa 中设置响应头
            [...responseHeaders.keys()].forEach(name => {
                ctx.set(name, responseHeaders.get(name));
            });
            ctx.body = body
            ctx.status = resp.status;
            return
        } else {
            resp = await axiosInstance.request({
                url: fullUrl,
                method: ctx.request.method,
                headers,
                data: ctx.request.body
            });
        }

        cache[fullUrl] = resp.data;
        console.log(resp)
        // 返回响应头
        const responseHeaders = resp.headers;
        Object.keys(responseHeaders).forEach(name => {
            ctx.set(name, responseHeaders[name]);
        });
        ctx.body = resp.data;
        ctx.status = resp.status;
    }
});

const router = new Router()
// 获取缓存路径列表
router.get('/cachedPaths', ctx => {
    ctx.body = Object.keys(cache);
});

// 获取缓存值
router.get('/cacheValue', ctx => {
    const { path } = ctx.request.query;
    ctx.body = { data: setResponse[path] || cache[path], isSet: !!setResponse[path] };
});

// 设置缓存值
router.post('/setCache', ctx => {
    const { path, value } = ctx.request.body;
    cache[path] = value;
    ctx.status = 204;
});

// 取消设置
router.get('/cancelSet', ctx => {
    const { path } = ctx.request.query;
    delete setResponse[path];
    ctx.status = 204;
})

app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000);